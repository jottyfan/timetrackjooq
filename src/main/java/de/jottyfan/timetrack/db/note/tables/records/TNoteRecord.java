/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.timetrack.db.note.tables.records;


import de.jottyfan.timetrack.db.note.enums.EnumCategory;
import de.jottyfan.timetrack.db.note.enums.EnumNotetype;
import de.jottyfan.timetrack.db.note.tables.TNote;

import java.time.LocalDateTime;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record6;
import org.jooq.Row6;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TNoteRecord extends UpdatableRecordImpl<TNoteRecord> implements Record6<Integer, String, EnumCategory, EnumNotetype, String, LocalDateTime> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>note.t_note.pk</code>.
     */
    public void setPk(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>note.t_note.pk</code>.
     */
    public Integer getPk() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>note.t_note.title</code>.
     */
    public void setTitle(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>note.t_note.title</code>.
     */
    public String getTitle() {
        return (String) get(1);
    }

    /**
     * Setter for <code>note.t_note.category</code>.
     */
    public void setCategory(EnumCategory value) {
        set(2, value);
    }

    /**
     * Getter for <code>note.t_note.category</code>.
     */
    public EnumCategory getCategory() {
        return (EnumCategory) get(2);
    }

    /**
     * Setter for <code>note.t_note.notetype</code>.
     */
    public void setNotetype(EnumNotetype value) {
        set(3, value);
    }

    /**
     * Getter for <code>note.t_note.notetype</code>.
     */
    public EnumNotetype getNotetype() {
        return (EnumNotetype) get(3);
    }

    /**
     * Setter for <code>note.t_note.content</code>.
     */
    public void setContent(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>note.t_note.content</code>.
     */
    public String getContent() {
        return (String) get(4);
    }

    /**
     * Setter for <code>note.t_note.lastchange</code>.
     */
    public void setLastchange(LocalDateTime value) {
        set(5, value);
    }

    /**
     * Getter for <code>note.t_note.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return (LocalDateTime) get(5);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record6 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row6<Integer, String, EnumCategory, EnumNotetype, String, LocalDateTime> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    @Override
    public Row6<Integer, String, EnumCategory, EnumNotetype, String, LocalDateTime> valuesRow() {
        return (Row6) super.valuesRow();
    }

    @Override
    public Field<Integer> field1() {
        return TNote.T_NOTE.PK;
    }

    @Override
    public Field<String> field2() {
        return TNote.T_NOTE.TITLE;
    }

    @Override
    public Field<EnumCategory> field3() {
        return TNote.T_NOTE.CATEGORY;
    }

    @Override
    public Field<EnumNotetype> field4() {
        return TNote.T_NOTE.NOTETYPE;
    }

    @Override
    public Field<String> field5() {
        return TNote.T_NOTE.CONTENT;
    }

    @Override
    public Field<LocalDateTime> field6() {
        return TNote.T_NOTE.LASTCHANGE;
    }

    @Override
    public Integer component1() {
        return getPk();
    }

    @Override
    public String component2() {
        return getTitle();
    }

    @Override
    public EnumCategory component3() {
        return getCategory();
    }

    @Override
    public EnumNotetype component4() {
        return getNotetype();
    }

    @Override
    public String component5() {
        return getContent();
    }

    @Override
    public LocalDateTime component6() {
        return getLastchange();
    }

    @Override
    public Integer value1() {
        return getPk();
    }

    @Override
    public String value2() {
        return getTitle();
    }

    @Override
    public EnumCategory value3() {
        return getCategory();
    }

    @Override
    public EnumNotetype value4() {
        return getNotetype();
    }

    @Override
    public String value5() {
        return getContent();
    }

    @Override
    public LocalDateTime value6() {
        return getLastchange();
    }

    @Override
    public TNoteRecord value1(Integer value) {
        setPk(value);
        return this;
    }

    @Override
    public TNoteRecord value2(String value) {
        setTitle(value);
        return this;
    }

    @Override
    public TNoteRecord value3(EnumCategory value) {
        setCategory(value);
        return this;
    }

    @Override
    public TNoteRecord value4(EnumNotetype value) {
        setNotetype(value);
        return this;
    }

    @Override
    public TNoteRecord value5(String value) {
        setContent(value);
        return this;
    }

    @Override
    public TNoteRecord value6(LocalDateTime value) {
        setLastchange(value);
        return this;
    }

    @Override
    public TNoteRecord values(Integer value1, String value2, EnumCategory value3, EnumNotetype value4, String value5, LocalDateTime value6) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TNoteRecord
     */
    public TNoteRecord() {
        super(TNote.T_NOTE);
    }

    /**
     * Create a detached, initialised TNoteRecord
     */
    public TNoteRecord(Integer pk, String title, EnumCategory category, EnumNotetype notetype, String content, LocalDateTime lastchange) {
        super(TNote.T_NOTE);

        setPk(pk);
        setTitle(title);
        setCategory(category);
        setNotetype(notetype);
        setContent(content);
        setLastchange(lastchange);
        resetChangedOnNotNull();
    }
}
