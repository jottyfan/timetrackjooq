/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.timetrack.db.done.tables.records;


import de.jottyfan.timetrack.db.done.tables.TBilling;

import java.time.LocalDateTime;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record5;
import org.jooq.Row5;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TBillingRecord extends UpdatableRecordImpl<TBillingRecord> implements Record5<LocalDateTime, Integer, String, String, String> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>done.t_billing.lastchange</code>.
     */
    public void setLastchange(LocalDateTime value) {
        set(0, value);
    }

    /**
     * Getter for <code>done.t_billing.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return (LocalDateTime) get(0);
    }

    /**
     * Setter for <code>done.t_billing.pk</code>.
     */
    public void setPk(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>done.t_billing.pk</code>.
     */
    public Integer getPk() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>done.t_billing.name</code>.
     */
    public void setName(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>done.t_billing.name</code>.
     */
    public String getName() {
        return (String) get(2);
    }

    /**
     * Setter for <code>done.t_billing.shortcut</code>.
     */
    public void setShortcut(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>done.t_billing.shortcut</code>.
     */
    public String getShortcut() {
        return (String) get(3);
    }

    /**
     * Setter for <code>done.t_billing.csskey</code>.
     */
    public void setCsskey(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>done.t_billing.csskey</code>.
     */
    public String getCsskey() {
        return (String) get(4);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record5 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row5<LocalDateTime, Integer, String, String, String> fieldsRow() {
        return (Row5) super.fieldsRow();
    }

    @Override
    public Row5<LocalDateTime, Integer, String, String, String> valuesRow() {
        return (Row5) super.valuesRow();
    }

    @Override
    public Field<LocalDateTime> field1() {
        return TBilling.T_BILLING.LASTCHANGE;
    }

    @Override
    public Field<Integer> field2() {
        return TBilling.T_BILLING.PK;
    }

    @Override
    public Field<String> field3() {
        return TBilling.T_BILLING.NAME;
    }

    @Override
    public Field<String> field4() {
        return TBilling.T_BILLING.SHORTCUT;
    }

    @Override
    public Field<String> field5() {
        return TBilling.T_BILLING.CSSKEY;
    }

    @Override
    public LocalDateTime component1() {
        return getLastchange();
    }

    @Override
    public Integer component2() {
        return getPk();
    }

    @Override
    public String component3() {
        return getName();
    }

    @Override
    public String component4() {
        return getShortcut();
    }

    @Override
    public String component5() {
        return getCsskey();
    }

    @Override
    public LocalDateTime value1() {
        return getLastchange();
    }

    @Override
    public Integer value2() {
        return getPk();
    }

    @Override
    public String value3() {
        return getName();
    }

    @Override
    public String value4() {
        return getShortcut();
    }

    @Override
    public String value5() {
        return getCsskey();
    }

    @Override
    public TBillingRecord value1(LocalDateTime value) {
        setLastchange(value);
        return this;
    }

    @Override
    public TBillingRecord value2(Integer value) {
        setPk(value);
        return this;
    }

    @Override
    public TBillingRecord value3(String value) {
        setName(value);
        return this;
    }

    @Override
    public TBillingRecord value4(String value) {
        setShortcut(value);
        return this;
    }

    @Override
    public TBillingRecord value5(String value) {
        setCsskey(value);
        return this;
    }

    @Override
    public TBillingRecord values(LocalDateTime value1, Integer value2, String value3, String value4, String value5) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TBillingRecord
     */
    public TBillingRecord() {
        super(TBilling.T_BILLING);
    }

    /**
     * Create a detached, initialised TBillingRecord
     */
    public TBillingRecord(LocalDateTime lastchange, Integer pk, String name, String shortcut, String csskey) {
        super(TBilling.T_BILLING);

        setLastchange(lastchange);
        setPk(pk);
        setName(name);
        setShortcut(shortcut);
        setCsskey(csskey);
        resetChangedOnNotNull();
    }
}
