/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.timetrack.db.done.tables;


import de.jottyfan.timetrack.db.done.Done;
import de.jottyfan.timetrack.db.done.tables.records.VDoneRecord;

import java.time.LocalDateTime;
import java.util.function.Function;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Function8;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Records;
import org.jooq.Row8;
import org.jooq.Schema;
import org.jooq.SelectField;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VDone extends TableImpl<VDoneRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>done.v_done</code>
     */
    public static final VDone V_DONE = new VDone();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VDoneRecord> getRecordType() {
        return VDoneRecord.class;
    }

    /**
     * The column <code>done.v_done.fk_done</code>.
     */
    public final TableField<VDoneRecord, Integer> FK_DONE = createField(DSL.name("fk_done"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>done.v_done.fk_login</code>.
     */
    public final TableField<VDoneRecord, Integer> FK_LOGIN = createField(DSL.name("fk_login"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>done.v_done.time_from</code>.
     */
    public final TableField<VDoneRecord, LocalDateTime> TIME_FROM = createField(DSL.name("time_from"), SQLDataType.LOCALDATETIME(6), this, "");

    /**
     * The column <code>done.v_done.time_until</code>.
     */
    public final TableField<VDoneRecord, LocalDateTime> TIME_UNTIL = createField(DSL.name("time_until"), SQLDataType.LOCALDATETIME(6), this, "");

    /**
     * The column <code>done.v_done.project_name</code>.
     */
    public final TableField<VDoneRecord, String> PROJECT_NAME = createField(DSL.name("project_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>done.v_done.module_name</code>.
     */
    public final TableField<VDoneRecord, String> MODULE_NAME = createField(DSL.name("module_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>done.v_done.job_name</code>.
     */
    public final TableField<VDoneRecord, String> JOB_NAME = createField(DSL.name("job_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>done.v_done.login</code>.
     */
    public final TableField<VDoneRecord, String> LOGIN = createField(DSL.name("login"), SQLDataType.CLOB, this, "");

    private VDone(Name alias, Table<VDoneRecord> aliased) {
        this(alias, aliased, null);
    }

    private VDone(Name alias, Table<VDoneRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view());
    }

    /**
     * Create an aliased <code>done.v_done</code> table reference
     */
    public VDone(String alias) {
        this(DSL.name(alias), V_DONE);
    }

    /**
     * Create an aliased <code>done.v_done</code> table reference
     */
    public VDone(Name alias) {
        this(alias, V_DONE);
    }

    /**
     * Create a <code>done.v_done</code> table reference
     */
    public VDone() {
        this(DSL.name("v_done"), null);
    }

    public <O extends Record> VDone(Table<O> child, ForeignKey<O, VDoneRecord> key) {
        super(child, key, V_DONE);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : Done.DONE;
    }

    @Override
    public VDone as(String alias) {
        return new VDone(DSL.name(alias), this);
    }

    @Override
    public VDone as(Name alias) {
        return new VDone(alias, this);
    }

    @Override
    public VDone as(Table<?> alias) {
        return new VDone(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public VDone rename(String name) {
        return new VDone(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VDone rename(Name name) {
        return new VDone(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public VDone rename(Table<?> name) {
        return new VDone(name.getQualifiedName(), null);
    }

    // -------------------------------------------------------------------------
    // Row8 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row8<Integer, Integer, LocalDateTime, LocalDateTime, String, String, String, String> fieldsRow() {
        return (Row8) super.fieldsRow();
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Function)}.
     */
    public <U> SelectField<U> mapping(Function8<? super Integer, ? super Integer, ? super LocalDateTime, ? super LocalDateTime, ? super String, ? super String, ? super String, ? super String, ? extends U> from) {
        return convertFrom(Records.mapping(from));
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Class,
     * Function)}.
     */
    public <U> SelectField<U> mapping(Class<U> toType, Function8<? super Integer, ? super Integer, ? super LocalDateTime, ? super LocalDateTime, ? super String, ? super String, ? super String, ? super String, ? extends U> from) {
        return convertFrom(toType, Records.mapping(from));
    }
}
